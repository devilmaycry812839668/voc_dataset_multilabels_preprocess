import tensorflow as tf
import numpy as np
from resolve_labels import all_files


pic_path="./VOC2007/train_val/VOCdevkit/VOC2007/JPEGImages"
#print(pic_path)

#all pictures name, string, *****.jpg
pic_list=tf.gfile.ListDirectory(pic_path)
#print(pic_list)

#filter the jpg files
pic_list=[i for i in pic_list if i.endswith('.jpg') ]
#print(pic_list)


#achieve new height and weight through resizing picture 
#new height equal 224 pixels, new weight equal 224 pixels
def preprocess(image_raw_data):
    # function input ,  image_raw_data is a tensor, here is placeholder
    """
    image_raw_data = tf.gfile.FastGFile(pic_path+"/"+pic, 'rb').read()
    #image_raw_data=tf.read_file(pic_path+"/"+pic)
    """
    #decode one picture which type is jpeg
    #just one image read process finished 
    img_data = tf.image.decode_jpeg(image_raw_data)    #get tensor

    #range for 0 to 255  convert into range for 0.0to1.0
    image_float = tf.image.convert_image_dtype(img_data, tf.float32)

    #resize picture
    img_resized = tf.image.resize_images(image_float, [224, 224], method=0)
    return img_resized


def _float32_feature(value):
    return tf.train.Feature(float_list=tf.train.FloatList(value=[value]))

def _int64_feature(value):
    return tf.train.Feature(int64_list=tf.train.Int64List(value=[value]))

def _bytes_feature(value):
    return tf.train.Feature(bytes_list=tf.train.BytesList(value=[value]))


file_out='./file_out/file_out.tfrecords'
writer=tf.python_io.TFRecordWriter(file_out)


#get labels for all pictures
def resolve_label():
    pic_obj_list=all_files()
    pic_obj_dict={}
    for pic in pic_obj_list:
        labels=[0.0]*20
        for sub_obj in pic.object_list:
            labels[ sub_obj.index ] = 1.0
        pic_obj_dict[pic.index]=labels
    # picture numbder,for example 000648 is the key of dict
    # labels is the value of dict which is [0.0, 1.0, 0.0, 1.0, 0.0]
    return pic_obj_dict

labels_dict=resolve_label()


with tf.Session() as sess:
    img_data_placeholder=tf.placeholder(tf.string )
    img_resized=preprocess(img_data_placeholder)


    for pic in pic_list:  #[0:1]:
        image_raw_data = tf.gfile.FastGFile(pic_path+"/"+pic, 'rb').read()
        #image_raw_data=tf.read_file(pic_path+"/"+pic)

        #get resized picture, numpy array
        img=sess.run(img_resized, feed_dict={img_data_placeholder:image_raw_data})

        #serialized numpy array to string 
        img=img.tostring()

        #define tensorflow tfrecoder example object
        example=tf.train.Example(features=tf.train.Features(feature={
                       #'y':_int64_feature(1),
                       #'x':_float32_feature(0.1),
                       'img':_bytes_feature(img),
                       'label':_bytes_feature( np.array(labels_dict[ pic[:-4] ], dtype=np.float32).tostring() )
                       }))
        #print(img)
        #print(labels_dict[ pic[:-4] ])

        #write string object to recoder file which represent tfrecoder example object  
        writer.write(example.SerializeToString())
    writer.close()


