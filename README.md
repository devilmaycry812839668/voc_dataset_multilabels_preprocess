# VOC_DATASET_MultiLabels_Preprocess

#### 介绍
多标签分类任务，VOC2007,VOC2012数据集，进行图像数据的预处理



###  运行环境
python=3.7
tensorflow=1.14



#### 使用说明

1.  共两个Python文件， img_label_preprocess.py  和  resolve_labels.py
2.  待处理的VOC文件存放路径为:
      ./VOC2007/train_val/VOCdevkit/VOC2007/JPEGImages 
      ./VOC2007/train_val/VOCdevkit/VOC2007/Annotations
3.  resolve_labels.py 文件将 ./VOC2007/train_val/VOCdevkit/VOC2007/Annotations 中的 .xml 文件进行解析，获得不同标号下图片所对应的长宽，图中所含物体的名称及其坐标与物体长宽。如：
PicClass(index='007417', width='500', height='269', object_list=[ObjectClass(index=7, name='dog', xmin='40', ymin='67', xmax='306', ymax='208'), ObjectClass(index=10, name='cat', xmin='304', ymin='105', xmax='451', ymax='211'), ObjectClass(index=14, name='sofa', xmin='1', ymin='2', xmax='499', ymax='269')])
4.  img_label_preprocess.py 将./VOC2007/train_val/VOCdevkit/VOC2007/JPEGImages 中的图片进行预处理，即，resize 到（224,224）大小，并调用 resolve_labels.py 文件将图片中对应的物体进行编码作为多标签分类的标签信息。最后，将预处理后的图片及其对应的标签存在 file_out/file_out.tfrecords文件中。
label形式为 [0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0]。
5. resolve_labels.py 可以单独使用， img_label_preprocess.py则必须调用resolve_labels.py才可使用。







